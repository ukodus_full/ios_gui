//
//  NumberPickViewController.swift
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 23.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

import UIKit

class NumberPickViewController: UIViewController {
	
	@IBOutlet weak var closeButton: UIButton!
	@IBOutlet weak var buttonsView: UIView!
	
	var numberButtons = [EntryButton]()
	
	let topSpace = UILayoutGuide()
	let bottomSpace = UILayoutGuide()
	
	var buttonToSet: EntryButton? = nil
	
	func buttonTagToCharacter(theTag: Int)-> Character {
		switch theTag {
		   case 0: return " ";
		   case 1: return "1";
		   case 2: return "2";
		   case 3: return "3";
		   case 4: return "4";
		   case 5: return "5";
		   case 6: return "6";
		   case 7: return "7";
		   case 8: return "8";
		   case 9: return "9";
			
		   default:
			   assert(false) // Bug
		}
	}
	
	@IBAction func numberButtonActionTriggered(sender: EntryButton) {
		buttonToSet?.setAsPlayer(theNewValue: buttonTagToCharacter(theTag: sender.tag))
		closePopUp(sender)
	}
	
	private func prepareNumberButtons() {
		
		///////////////////////////////////////////////////////////////////
		// Part 1: Create all buttons
		
		for index in 0 ..< 10 {
			let button = EntryButton(type: .system)
			self.numberButtons.append(button)
			
			button.tag = index
			button.addTarget(
				self,
				action: #selector(numberButtonActionTriggered(sender:)),
				for: .touchUpInside)
			button.setAsPlayer(theNewValue: buttonTagToCharacter(theTag: button.tag))
			button.translatesAutoresizingMaskIntoConstraints = false
			
			self.buttonsView.addSubview(button)
		}
		
		///////////////////////////////////////////////////////////////////
		// Part 2: Set all constraints
		
		let sideMargin = self.buttonsView.frame.width * 0.15 // 15% of the buttonView's frame's width
		
		let button0 = self.numberButtons[0]
		let button1 = self.numberButtons[1]
		
		// Make button0 a square (will be the template for all others)
		self.view.addConstraint(NSLayoutConstraint(
			item: button0, attribute: NSLayoutAttribute.width,
			relatedBy: NSLayoutRelation.equal,
			toItem: button0, attribute: NSLayoutAttribute.height,
			multiplier: 1, constant: 0.0))
		
		// Set button1's left margin
		self.view.addConstraint(NSLayoutConstraint(
			item: button1, attribute: NSLayoutAttribute.leading,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.buttonsView, attribute: NSLayoutAttribute.leading,
			multiplier: 1, constant: sideMargin))
		
		for index in 1 ..< 10 {
			let button = self.numberButtons[index]
			
			// All entry buttons should have the same width and same height:
			// Same width:
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.width,
				relatedBy: NSLayoutRelation.equal,
				toItem: button0, attribute: NSLayoutAttribute.width,
				multiplier: 1, constant: 0.0))
			// Same height:
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.height,
				relatedBy: NSLayoutRelation.equal,
				toItem: button0, attribute: NSLayoutAttribute.height,
				multiplier: 1, constant: 0.0))
		}
		
		let largeDistance: CGFloat = 7.0
		
		// Prepare the first row
		for index in [2, 3] {
			let button = self.numberButtons[index]
			
			// Set top position
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.top,
				relatedBy: NSLayoutRelation.equal,
				toItem: button1, attribute: NSLayoutAttribute.top,
				multiplier: 1, constant: 0.0))
			
			// Set spacing
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.leading,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.numberButtons[index - 1], attribute: NSLayoutAttribute.trailing,
				multiplier: 1, constant: largeDistance))
		}
		
		// Set the right margin of the top-right button
		self.view.addConstraint(NSLayoutConstraint(
			item: self.numberButtons[3], attribute: NSLayoutAttribute.trailing,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.buttonsView, attribute: NSLayoutAttribute.trailing,
			multiplier: 1, constant: -sideMargin))
		
		// Prepare the first column
		for index in [4, 7] {
			let button = self.numberButtons[index]
			
			// Set left alignment
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.leading,
				relatedBy: NSLayoutRelation.equal,
				toItem: button1, attribute: NSLayoutAttribute.leading,
				multiplier: 1, constant: 0.0))
			
			// Set spacing
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.top,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.numberButtons[index - 3], attribute: NSLayoutAttribute.bottom,
				multiplier: 1, constant: largeDistance))
		}
		
		// Set all remaining buttons
		for index in [5, 6, 8, 9] {
			let button = self.numberButtons[index]
			
			// Set left alignment
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.leading,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.numberButtons[index - 3], attribute: NSLayoutAttribute.leading,
				multiplier: 1, constant: 0.0))
			
			// Set top position
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.top,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.numberButtons[index - ((index - 1) % 3)], attribute: NSLayoutAttribute.top,
				multiplier: 1, constant: 0.0))
		}
		
		// Set the position of button0		
		// Set left alignment
		self.view.addConstraint(NSLayoutConstraint(
			item: button0, attribute: NSLayoutAttribute.leading,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.numberButtons[8], attribute: NSLayoutAttribute.leading,
			multiplier: 1, constant: 0.0))
		
		// Set top position
		self.view.addConstraint(NSLayoutConstraint(
			item: button0, attribute: NSLayoutAttribute.top,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.numberButtons[8], attribute: NSLayoutAttribute.bottom,
			multiplier: 1, constant: largeDistance))
		
		// Centralize the whole thing using layout guides
		self.view.addLayoutGuide(topSpace)
		self.view.addLayoutGuide(bottomSpace)
		
		// Place the topSpace between top of the buttonsView and button1
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.top,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.buttonsView, attribute: NSLayoutAttribute.top,
			multiplier: 1, constant: 0.0))
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.bottom,
			relatedBy: NSLayoutRelation.equal,
			toItem: button1, attribute: NSLayoutAttribute.top,
			multiplier: 1, constant: 0.0))
		
		// Place the bottomSpace between button0 and the bottom of the buttonsView
		self.view.addConstraint(NSLayoutConstraint(
			item: bottomSpace, attribute: NSLayoutAttribute.top,
			relatedBy: NSLayoutRelation.equal,
			toItem: button0, attribute: NSLayoutAttribute.bottom,
			multiplier: 1, constant: 0.0))
		self.view.addConstraint(NSLayoutConstraint(
			item: bottomSpace, attribute: NSLayoutAttribute.bottom,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.buttonsView, attribute: NSLayoutAttribute.bottom,
			multiplier: 1, constant: 0.0))
		
		// Make the topSpace and the bottomSpace have the same height
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.height,
			relatedBy: NSLayoutRelation.equal,
			toItem: bottomSpace, attribute: NSLayoutAttribute.height,
			multiplier: 1, constant: 0.0))
	}
	
	@IBAction func closePopUp(_ sender: Any) {
		self.view.removeFromSuperview()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.prepareNumberButtons()
		
		buttonsView.isUserInteractionEnabled = true
		
		self.view.bringSubview(toFront: buttonsView)
		self.view.bringSubview(toFront: closeButton)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}
