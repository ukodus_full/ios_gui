//
//  EntryButtons.swift
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 22.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

import UIKit

class EntryButton : UIButton {
	
	static let playerTextColor      = UIColor.black
	static let solverTextColor      = UIColor(red: 201/255, green: 0/255, blue: 0/255, alpha: 1.0)
	static let theBackgroundColor   = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0)
	static let theShadowColor       = UIColor.darkGray
	
	var entryValue: Character = " "
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		// Set up runtime button properties:
		layer.borderWidth = 0.0
		layer.cornerRadius = 0.0
		layer.backgroundColor = EntryButton.theBackgroundColor.cgColor
		
		layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
		layer.shadowColor = EntryButton.theShadowColor.cgColor
		layer.shadowOpacity = 1.0
		layer.shadowRadius = 0.0
		
		setAsPlayer(theNewValue: " ")
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setEntryValue(theNewValue: Character) {
		self.entryValue = theNewValue
		setTitle(String(self.entryValue), for: .normal)
	}
	
	func setAsPlayer(theNewValue: Character) {
		setEntryValue(theNewValue: theNewValue)
		setTitleColor(EntryButton.playerTextColor, for: .normal)
	}
	
	func setAsSolver(theNewValue: Character) {
		setEntryValue(theNewValue: theNewValue)
		setTitleColor(EntryButton.solverTextColor, for: .normal)
	}
	
	func isSet() -> Bool {
		return (self.entryValue != " ")
	}
	
	func charForBoard() -> Character {
		switch self.entryValue {
		   case " ": return "_"
   		case "1": return "0"
	   	case "2": return "1"
		   case "3": return "2"
		   case "4": return "3"
	    	case "5": return "4"
		   case "6": return "5"
		   case "7": return "6"
		   case "8": return "7"
		   case "9": return "8"
			
		   default:
		    	assert(false) // Bug
		}
	}
	
	static func boardCharToButtonChar(ch: Character) -> Character {
		switch ch {
		   case "_": return " "
		   case "0": return "1"
		   case "1": return "2"
		   case "2": return "3"
		   case "3": return "4"
		   case "4": return "5"
		   case "5": return "6"
		   case "6": return "7"
		   case "7": return "8"
	   	case "8": return "9"
			
	   	default:
	   		assert(false) // Bug
		}
	}
	
}
