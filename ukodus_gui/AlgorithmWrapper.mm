//
//  AlgorithmWrapper.mm
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 21.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

#import "AlgorithmWrapper.h"

#include <utils/time_printer.hpp>

#include <interface/single_string_solver.hpp>

#include <sstream>

@implementation Solution

-(instancetype) initWithInitialState: (NSString *) initialState
{
	self = [super init];
	
	if (self)
	{
		std::string mutable_board = initialState.UTF8String;
		
		std::pair<AL::Algorithm::RetValue, std::string> const result =
		   IF::StringSolver::parse_and_try_solve(mutable_board);
		
		switch (result.first.code)
		{
			case AL::Algorithm::TrySolveRetCode::initial_state_illegal:
				_retCode = INITIAL_STATE_ILLEGAL; break;
			case AL::Algorithm::TrySolveRetCode::solved:
				_retCode = SOLVED; break;
			case AL::Algorithm::TrySolveRetCode::proved_unfeasibility:
				_retCode = PROVED_UNFEASIBILITY; break;
			case AL::Algorithm::TrySolveRetCode::gave_up:
				_retCode = GAVE_UP; break;
			default:
				assert(false); // Bug
		}
		
		_timeInNanoseconds = result.first.time.count();
		
		if (_retCode == SOLVED)
		{
		   _board = [NSString stringWithUTF8String: result.second.c_str()];
		}
	}
	
	return self;
}

-(NSString *) getTimePrettyString
{
	std::ostringstream oss;
	UT::OStream out(oss);
	out << UT::PrettyTime(std::chrono::nanoseconds(self.timeInNanoseconds));
	return [NSString stringWithUTF8String: oss.str().c_str()];
}

-(NSString *) getRetCodePrettyString
{
	switch (self.retCode)
	{
		case INITIAL_STATE_ILLEGAL:
			return @"Initial state was illegal!";
		case SOLVED:
			return @"Done.";
		case PROVED_UNFEASIBILITY:
			return @"Done. (Proved unfeasibility.)";
		case GAVE_UP:
			return @"I give up!";
		default:
			assert(false); // Bug
	}
}

-(NSString *) getDifficultyPrettyString
{
	if ((self.retCode == INITIAL_STATE_ILLEGAL) || (self.retCode == GAVE_UP)) { return nil; }
	
	std::chrono::seconds const threshold(1);
	
	// Say the instance was easy if it took less than a second to solve. Hard otherwise:
	if (std::chrono::nanoseconds(self.timeInNanoseconds) < threshold)
	{
		return @"Piece of cake.";
	}
	else
	{
		return @"That was a good one!";
	}
}

@end
