//
//  AlgorithmWrapper.h
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 21.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AlgorithmRetCode)
{
	INITIAL_STATE_ILLEGAL  = 0,
	SOLVED                 = 1,
	PROVED_UNFEASIBILITY   = 2,
	GAVE_UP                = 3
};

@interface Solution : NSObject

@property AlgorithmRetCode retCode;
@property NSUInteger timeInNanoseconds;
@property NSString * board;

-(instancetype) initWithInitialState: (NSString *) initialState;

-(NSString *) getTimePrettyString;
-(NSString *) getRetCodePrettyString;
-(NSString *) getDifficultyPrettyString;

@end
