//
//  ResultTextViewController.swift
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 23.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

import UIKit

class ResultTextViewController: UIViewController {

	@IBOutlet weak var resultLabel: UILabel!
	@IBOutlet weak var closeButton: UIButton!
	@IBOutlet weak var resultLabelView: UIView!
	
	@IBAction func closePopUp(_ sender: Any) {
		self.view.removeFromSuperview()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		resultLabelView.isUserInteractionEnabled = true
		resultLabel.isUserInteractionEnabled = true
		
		self.view.bringSubview(toFront: resultLabelView)
		self.view.bringSubview(toFront: resultLabel)
		self.view.bringSubview(toFront: closeButton)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}
