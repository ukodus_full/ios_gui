//
//  ViewController.swift
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 19.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var solveButton: NiceButton!
	@IBOutlet weak var clearButton: NiceButton!
	@IBOutlet weak var titleLabel: UILabel!
	
	var entryButtons = [EntryButton]()
	
	let topSpace = UILayoutGuide()
	let bottomSpace = UILayoutGuide()
	
	private func prepareEntryButtons() {
		// The following are currently hard-coded constants, but could be
		// set dinamically in a future version (e.g. if we want to support
		// boards of size 6x6, 20x20 and so on):
		let blockWidth = 3
		let blockHeight = 3
		let numAllowedValues = blockWidth * blockHeight
		let numEntries = numAllowedValues * numAllowedValues
		
		///////////////////////////////////////////////////////////////////
		// Part 1: Create all buttons
		
		for index in 0 ..< numEntries {
			let button = EntryButton(type: .system)
			self.entryButtons.append(button)
			
			button.tag = index
			button.addTarget(
				self,
				action: #selector(entryButtonActionTriggered(sender:)),
				for: .touchUpInside)
			button.setAsPlayer(theNewValue: " ")
			button.translatesAutoresizingMaskIntoConstraints = false

			self.view.addSubview(button)
		}
		
		///////////////////////////////////////////////////////////////////
		// Part 2: Set all constraints
		
		let sideMargin = self.view.frame.width * 0.03 // 3% of the view's frame's width
		let firstButton = self.entryButtons[0]
		
		// Make the first button a square (will be the template for all others)
		self.view.addConstraint(NSLayoutConstraint(
			item: firstButton, attribute: NSLayoutAttribute.width,
			relatedBy: NSLayoutRelation.equal,
			toItem: firstButton, attribute: NSLayoutAttribute.height,
			multiplier: 1, constant: 0.0))
		
		// Set the first button's left margin
		self.view.addConstraint(NSLayoutConstraint(
			item: firstButton, attribute: NSLayoutAttribute.leading,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.view, attribute: NSLayoutAttribute.leading,
			multiplier: 1, constant: sideMargin))

		for index in 1 ..< numEntries {
			let button = self.entryButtons[index]
			
			// All entry buttons should have the same width and same height:
			// Same width:
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.width,
				relatedBy: NSLayoutRelation.equal,
				toItem: firstButton, attribute: NSLayoutAttribute.width,
				multiplier: 1, constant: 0.0))
			// Same height:
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.height,
				relatedBy: NSLayoutRelation.equal,
				toItem: firstButton, attribute: NSLayoutAttribute.height,
				multiplier: 1, constant: 0.0))
		}
		
		let smallDistance: CGFloat = 4.0
		let largeDistance: CGFloat = 7.0
		
		// Prepare the first row
		for column in 1 ..< numAllowedValues {
			let button = self.entryButtons[column]
			
			// Set top position
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.top,
				relatedBy: NSLayoutRelation.equal,
				toItem: firstButton, attribute: NSLayoutAttribute.top,
				multiplier: 1, constant: 0.0))
			
			// Set spacing
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.leading,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.entryButtons[column - 1], attribute: NSLayoutAttribute.trailing,
				multiplier: 1, constant: (column % blockWidth == 0 ? largeDistance : smallDistance)))
		}
		
		// Set the right margin of the top-right button
		self.view.addConstraint(NSLayoutConstraint(
			item: self.entryButtons[numAllowedValues - 1], attribute: NSLayoutAttribute.trailing,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.view, attribute: NSLayoutAttribute.trailing,
			multiplier: 1, constant: -sideMargin))
		
		// Prepare the first column
		for row in 1 ..< numAllowedValues {
			let button = self.entryButtons[row * numAllowedValues]
			
			// Set left alignment
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.leading,
				relatedBy: NSLayoutRelation.equal,
				toItem: firstButton, attribute: NSLayoutAttribute.leading,
				multiplier: 1, constant: 0.0))
			
			// Set spacing
			self.view.addConstraint(NSLayoutConstraint(
				item: button, attribute: NSLayoutAttribute.top,
				relatedBy: NSLayoutRelation.equal,
				toItem: self.entryButtons[(row - 1) * numAllowedValues], attribute: NSLayoutAttribute.bottom,
				multiplier: 1, constant: (row % blockHeight == 0 ? largeDistance : smallDistance)))
		}
		
		// Set all remaining buttons
		for row in 1 ..< numAllowedValues {
			for column in 1 ..< numAllowedValues {
				let button = self.entryButtons[(row * numAllowedValues) + column]
				
				// Set left alignment
				self.view.addConstraint(NSLayoutConstraint(
					item: button, attribute: NSLayoutAttribute.leading,
					relatedBy: NSLayoutRelation.equal,
					toItem: self.entryButtons[column], attribute: NSLayoutAttribute.leading,
					multiplier: 1, constant: 0.0))
				
				// Set top position
				self.view.addConstraint(NSLayoutConstraint(
					item: button, attribute: NSLayoutAttribute.top,
					relatedBy: NSLayoutRelation.equal,
					toItem: self.entryButtons[row * numAllowedValues], attribute: NSLayoutAttribute.top,
					multiplier: 1, constant: 0.0))
			}
		}
		
		// Centralize the whole thing using layout guides
		self.view.addLayoutGuide(topSpace)
		self.view.addLayoutGuide(bottomSpace)
		
		// Place the topSpace between the titleLabel and the first button
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.top,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.titleLabel, attribute: NSLayoutAttribute.bottom,
			multiplier: 1, constant: 0.0))
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.bottom,
			relatedBy: NSLayoutRelation.equal,
			toItem: firstButton, attribute: NSLayoutAttribute.top,
			multiplier: 1, constant: 0.0))
		
		// Place the bottomSpace between the last button and the solve button
		self.view.addConstraint(NSLayoutConstraint(
			item: bottomSpace, attribute: NSLayoutAttribute.top,
			relatedBy: NSLayoutRelation.equal,
			toItem: self.entryButtons[numEntries - 1], attribute: NSLayoutAttribute.bottom,
			multiplier: 1, constant: 0.0))
		self.view.addConstraint(NSLayoutConstraint(
			item: bottomSpace, attribute: NSLayoutAttribute.bottom,
			relatedBy: NSLayoutRelation.equal,
			toItem: solveButton, attribute: NSLayoutAttribute.top,
			multiplier: 1, constant: 0.0))
		
		// Make the topSpace and the bottomSpace have the same height
		self.view.addConstraint(NSLayoutConstraint(
			item: topSpace, attribute: NSLayoutAttribute.height,
			relatedBy: NSLayoutRelation.equal,
			toItem: bottomSpace, attribute: NSLayoutAttribute.height,
			multiplier: 1, constant: 0.0))
	}
	
	@IBAction func solveActionTriggered() {
		var initialBoard = ""
		
		for button in self.entryButtons {
			initialBoard += String(button.charForBoard())
		}
		
		let solution = Solution(initialState: initialBoard)!
		
		if (solution.retCode == AlgorithmRetCode.SOLVED) {
			for (index, boardChar) in solution.board!.characters.enumerated() {
				let button = self.entryButtons[index]
				
				if (!button.isSet()) {
					button.setAsSolver(theNewValue: EntryButton.boardCharToButtonChar(ch: boardChar))
				}
			}
		}
		
		var resultText = solution.getRetCodePrettyString()!
			
		if (solution.retCode != AlgorithmRetCode.INITIAL_STATE_ILLEGAL) {
			resultText += "\nTook: \(solution.getTimePrettyString()!)."
		}
		
		let difficultyString = solution.getDifficultyPrettyString()
		
		if (difficultyString != nil)
		{
			resultText += "\n\n\(difficultyString!)"
		}
		
		openResultTextPopup(text: resultText)
	}
	
	@IBAction func clearActionTriggered() {
		for button in self.entryButtons {
			button.setAsPlayer(theNewValue: " ")
		}
	}
	
	@IBAction func entryButtonActionTriggered(sender: EntryButton) {
		openNumberPickPopup(buttonToSet: sender)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.prepareEntryButtons()

		// Quickly set an example sudoku (for tests):
		//		let initialBoard =
		//		     "17__26___"
		//		   + "___578__1"
		//		   + "6_8___5__"
		//		   + "_307__15_"
		//		   + "8__1_5__3"
		//		   + "_51__360_"
		//			+ "__2___7_5"
		//			+ "5__380___"
		//			+ "___25__10"
		//
		//		assert(initialBoard.characters.count == 81)
		//
		//		for (index, boardChar) in initialBoard.characters.enumerated() {
		//			let button = self.entryButtons[index]
		//			button.setAsPlayer(
		//          theNewValue: EntryButton.boardCharToButtonChar(ch: boardChar))
		//		}
	}

	override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
	
	func openResultTextPopup(text: String) {
		let child = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
			withIdentifier: "resultTextPopUpID") as! ResultTextViewController
		
		self.addChildViewController(child)
		
		child.view.frame = self.view.frame
		child.resultLabel.text = text
		
		self.view.addSubview(child.view)
		child.didMove(toParentViewController: self)
	}
	
	func openNumberPickPopup(buttonToSet: EntryButton) {
		let child = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
			withIdentifier: "numberPickPopUpID") as! NumberPickViewController
		
		self.addChildViewController(child)
		
		child.view.frame = self.view.frame
		child.buttonToSet = buttonToSet

		self.view.addSubview(child.view)
		child.didMove(toParentViewController: self)
	}

}

