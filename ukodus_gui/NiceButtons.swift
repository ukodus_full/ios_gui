//
//  NiceButtons.swift
//  ukodus_gui
//
//  Created by Tomas Silveira Salles on 22.05.17.
//  Copyright © 2017 TomOn. All rights reserved.
//

import UIKit

class NiceButton : UIButton {
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		// Set up runtime button properties:
		layer.borderWidth = 1.5
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		layer.borderColor = isEnabled ? tintColor.cgColor : UIColor.darkGray.cgColor
		layer.cornerRadius = frame.height / 2
	}
	
}
