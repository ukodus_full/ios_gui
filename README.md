# Ukodus iOS GUI project

Ukodus is a reverse game of sudoku, where the player tries to set the board for
a sudoku as difficult as possible and then the machine has the task of solving 
it as quickly as possible.

This project uses the libraries from a C++ sibling project `Ukodus`, inside the
same project group `Ukodus Full` to implement an iOS app. In the app, only the
classic version (9x9, with row-constraints, column-constraints and 
block-constraints, using the digits 1 through 9) can be played, although some of
the code was already written in a slightly more general fashion, and the C++
libraries from `Ukodus` are very flexible.

The game is fairly simple and self-explanatory.

The files `AlgorithmWrapper.h/.mm` constitute the bridging code between C++ and
Objective-C, which is then used in the rest of the project, which is written
in Swift.
