//: Playground - noun: a place where people can play

import UIKit

var ch: Character = "a"

func unicodeNextCharacter(ch: Character) -> Character {
	return Character(String(describing: UnicodeScalar(UnicodeScalar(String(ch))!.value + 1)!))
}

print(unicodeNextCharacter(ch: ch))
